var express     = require('express');
var proveedorService     = require('./proveedor.service');

var route = express.Router(); 

route.get('/:id', function(req, res) {
    proveedorService.getProveedor(req.params.id).then(function(response){
        res.json(response);
    }, function(e){
        res.status(503).send(e.message);
    });
});

route.put('/', function(req, res){
    proveedorService.update(req.body).then(function(response){
        res.json(response);
    }, function(e){
        res.status(503).send(e.message);
    });
});

route.post('/', function(req, res){
    proveedorService.save(req.body).then(function(response){
        res.json(response);
    }, function(e){
        res.status(503).send(e.message);
    });
});

route.get('/all', function(req, res){
    proveedorService.getAll().then(function(response){
        res.json(response);
    }, function(e){
        res.status(503).send(e.message);
    });
});

route.delete('/:id', function(req, res){
    proveedorService.eliminarProveedor(req.params.id).then(function(msj){
        res.json(msj);
    }, function(e){
        res.status(503).send(e.message);
    });
});

module.exports = route;