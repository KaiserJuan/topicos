var Proveedor   = require('./proveedor.model'); // get our mongoose model
var mongoose    = require('mongoose');
var Promise = require('promise');

function proveedorService(){

	function validar(_v) {
		return _v && _v.nombre;
	}

	//Funcion para Guardar Proveedor
	this.save = function(_proveedor){
		return new Promise(function(resolve, reject){
			if(validar(_proveedor)){
				var proveedor = new Proveedor({
					_id: new mongoose.Types.ObjectId(),
					nombre: _proveedor.nombre,
				});
				proveedor.save(function(error){
					if(error)
						reject(new Error('Ocurrió un error al guardar el Proveedor en la BD.'));		
					resolve(proveedor);		
				});
			}else
				reject(new Error('Proveedor no valido en la BD.'));
		});
	};

	//Funcion para Actualizar Proveedor
	this.update = function (_proveedor){
		return new Promise(function(resolve, reject){
			Proveedor.findById(_proveedor._id, function (err, proveedor) {
				if (err) 
					reject(new Error('Ocurrió un error al buscar el Proveedor en la BD.'));		
				else{
					proveedor.nombre = _proveedor.nombre;
					proveedor.save(function (err, proveedorN) {
					if (err) 
						reject(new Error('Ocurrió un error al actualizar el Proveedor en la BD.'));
					resolve(proveedorN);
					});
				}
			});
			
		});
	};

	//Funcion para Buscar todos los proveedors
	this.getAll = function(){
		return new Promise(function(resolve, reject){
			Proveedor.find(function(e, proveedors){
				if(e)
					reject(new Error('Ocurrió un error al buscar los Proveedors en la BD.'));
				else 
				 resolve(proveedors);
			})
		})
	}

	//Funcion para buscar un Proveedor
	this.getProveedor = function(_id){
		return new Promise(function(resolve, reject){
			Proveedor.findById({ id: _id , function(e, proveedor){
			if(e || proveedor.length == 0)
				reject(new Error('Nombre de proveedor o contraseña incorrectos.'));
			else resolve(proveedor);
			}});
		});
	};

	//Funcion para eliminar un Proveedor por ID
	this.eliminarProveedor = function(id){
		return new Promise(function(resolve, reject){
			Proveedor.remove({_id:id}, function(error){
				if(error)
					reject(new Error('Ocurrió un error al eliminar el Proveedor.'));
				resolve('Exito al eliminar!');
			});
		})
	}
	return this;
}

module.exports = new proveedorService();