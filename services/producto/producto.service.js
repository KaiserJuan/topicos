var Producto   = require('./producto.model'); // get our mongoose model
var mongoose    = require('mongoose');
var Promise = require('promise');

function productoService(){

	function validar(_v) {
		return _v && _v.nombre && _v.precio;
	}
	//Funcion para Guardar Producto
	this.save = function(_producto){
		return new Promise(function(resolve, reject){
			if(validar(_producto)){
				var producto= new Producto({
					_id: new mongoose.Types.ObjectId(),
					codigo: _producto.codigo,
					nombre: _producto.nombre,
					minStock: _producto.minStock,
					proveedor: _producto.proveedor
				});
				producto.save(function(error){
					if(error)
						reject(new Error('Ocurrió un error al guardar el Producto en la BD.'));		
					resolve(producto);		
				});
			}else
				reject(new Error('Producto no valido en la BD.'));
		});
	};

	//Funcion para Actualizar Producto
	this.update = function (_producto){
		return new Promise(function(resolve, reject){
			Producto.findById(_producto._id, function (err, producto) {
				if (err) 
					reject(new Error('Ocurrió un error al buscar el Producto en la BD.'));		
				else{
					producto.nombre = _producto.nombre;
					producto.codigo = _producto.codigo;
					producto.precio = _producto.precio;
					producto.minStock = _producto.minStock;
					producto.proveedor = _producto.proveedor;
					producto.save(function (err, productN) {
					if (err) 
						reject(new Error('Ocurrió un error al actualizar el Producto en la BD.'));
					resolve(productN);
					});
				}
			});
		});
	};

	//Funcion para Buscar todos los productos
	this.getAll = function(){
		return new Promise(function(resolve, reject){
			Producto.find(function(e, producto){
				if(e)
					reject(new Error('Ocurrió un error al buscar los Productos en la BD.'));
				else 
				 resolve(producto);
			})
		})
	}

	//Funcion para Buscar todos los productos, solo con nombre y precio
	this.getAll2 = function(){
		return new Promise(function(resolve, reject){
			Producto.find({'minStock': 10})
			.where('precio')
			.gt(100)
			.limit(10)
			.sort('-nombre')
			.select('nombre precio')
			.exec(function(e, producto){
				if(e)
					reject(new Error('Ocurrió un error al buscar los Productos en la BD.'));
				else 
				 resolve(producto);
			})
		})
	}

	//Funcion para buscar un Producto
	this.getProducto = function(_id){
		return new Promise(function(resolve, reject){
			Producto.findById({id: _id, function (e, producto) {
				if(e || producto === undefined)
					reject(new Error('El producto no existe'));
					else resolve(producto);
			}})
		});
	};

	//Funcion para buscar Productos por Nombre
	this.getProductoByNombre = function(nombre){
		return new Promise(function(resolve, reject){
			Producto.find({nombre : nombre}, function(e, producto){
			if(e || producto.length == 0)
				reject(new Error('No se encontro un producto con ese correo.'));
			else resolve(producto);
		});
		});
	};

	//Funcion para eliminar un Producto por ID
	this.eliminarProducto = function(id){
		return new Promise(function(resolve, reject){
			Producto.find({}, function(err, docs){
				if(err)
					reject(new Error('Ocurrió un error al buscar los Productos.'));
				else {
					Producto.findById(id, function(e, doc){
						if(e)
							reject(new Error('Ocurrió un error al buscar el Producto.'))
						else if(doc == null)
							reject(new Error('No se encontro el Producto en la BD'));
							else{
								Producto.remove({_id:id}, function(error){
									if(error)
										reject(new Error('Ocurrió un error al eliminar el Producto.'));
									resolve('Exito al eliminar!');
								})
							}
					});
				}
			});
		})
	}
	return this;
}

module.exports = new productoService();