var express     = require('express');
var productoService     = require('./producto.service');

var route = express.Router(); 

route.get('/:id', function(req, res) {
    productoService.getProducto(req.params.id).then(function(response){
        res.json(response);
    }, function(e){
        res.status(503).send(e.message);
    });
});

route.get('/:name', function(req, res) {
    productoService.getProductoByNombre(req.params.name).then(function(response){
        res.json(response);
    }, function(e){
        res.status(503).send(e.message);
    });
});

route.put('/', function(req, res){
    productoService.update(req.body).then(function(response){
        res.json(response);
    }, function(e){
        res.status(503).send(e.message);
    });
});

route.post('/', function(req, res){
    productoService.save(req.body).then(function(response){
        res.json(response);
    }, function(e){
        res.status(503).send(e.message);
    });
});

route.get('/all', function(req, res){
    productoService.getAll().then(function(users){
        res.json(users);
    }, function(e){
        res.status(503).send(e.message);
    });
});

route.delete('/:id', function(req, res){
    productoService.eliminarProducto(req.params.id).then(function(msj){
        res.json(msj);
    }, function(e){
        res.status(503).send(e.message);
    });
});

module.exports = route;